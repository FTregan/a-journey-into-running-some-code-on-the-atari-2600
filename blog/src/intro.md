# Introduction

Demo is a form of art where you code real time graphical things to hexib the beauty of what can be done with your hardware and your skills. (it is ok if you disagree with this definition).

In early 2019 I was browsing the results of latest demo parties on [pouet.net] to see what was the state of the  *scene* (I stopped following it in the late 90's), when I found about [this weird demo](https://www.pouet.net/prod.php?which=78489) called HARD 2632 which won first place in the "wild demo" compo ("wild" meaning that it does not fit in other demo competitions, like Atari, PC, 4k - less than 4097 bytes executables -, ...)

I ran this demo on a [Atari 2600 emulator](https://stella-emu.github.io/), and the fact that it actually won something was disturbing: it looked pretty weird, had no music, only one [very minimalist screen](https://www.youtube.com/watch?v=n812OOEHW8I) that looked more like a bug than a demo... But after some research, the story behind this wild composition is rather interesting.

![HARD 2632](images/hard2632.png)

In september of 2018 on the AtariAge forum a [discusion](https://atariage.com/forums/topic/282809-minimal-demo/) was proposed by SvOlli about what could be the minimal criteria for something to be considered a minimal demo for the Atari 2600, and how to make a minimal demo. I'll let you read the whole thread if you want, but they agreed that 32 bytes was a reasonnable minimal size (more precisely: the lower power of two sized ROM that could possibly host a demo that would match the criterias).

But the story doesn't end there. In the thread on the forum, someone linked about a 64 bytes Atari 2600 ROM home-made from diodes, and less than a month after starting the thread SvOlli made a [32 bytes version using DIP switchs](https://xayax.net/hard2632/) so one could make his/her own ROM with your own 32 byte demo, only toggling tiny switches (each switch is a bit in th ROM, 8x32=256 bits (or swtiches)). So this is what he entered the wild demo competition with: his custom 32 bytes dip switch Atari 2600 cartridge hosting a working 32 bytes Atari 2600 demo.

This expectidly won the wild demo competition and rised my interest in Atari 2600 programming.

I coded a few things, learned things about the Atari 2600, a few about myself, but wasn't really pleased with running the code on an emulator. I'll try documentation what I did on this blog, maybe it will also help me finishing this project some day.

---

Want to discuss about this chapter?
 * Via gitlab [this issue](https://gitlab.com/FTregan/a-journey-into-running-some-code-on-the-atari-2600/-/issues/1)
 * Via Twitter [@ftregan](https://twitter.com/ftregan)

Want to correct something?
 * Make a PR on [gitlab](https://gitlab.com/FTregan/a-journey-into-running-some-code-on-the-atari-2600)
 * DM me on [@ftregan](https://twitter.com/ftregan) 